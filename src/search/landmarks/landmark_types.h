#ifndef LANDMARKS_LANDMARK_TYPES_H
#define LANDMARKS_LANDMARK_TYPES_H

#include "../utils/hash.h"

#include <utility>

using lm_set = utils::HashSet<std::pair<int, int> >;

#endif
